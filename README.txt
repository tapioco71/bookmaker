BOOKMAKER
=========

BOOKMAKER is a little project born from the needs of a small parish to
compose an A5 booklet with sponsors to fund community events.

Usage example.
--------------

Create a text file with .dat extension, like page-01.dat . In this file place
a description like this:

#s(page-struct :name "Booklet First Page"
	       :number 1
	       :background-color "white"
	       :images-list (#s(image-struct :name "Foobar Reseller"
					     :row 1
					     :column 1 
					     :row-span 2
					     :column-span 2
					     :file-pathname #p"/path/to/foobar-reseller-pic.png")
			     #s(image-struct :name "Frob Terminator"
					     :row 3
					     :column 1 
					     :row-span 2
					     :column-span 2
					     :file-pathname #p"/path/to/frob-terminator-pic.png")))

this file holds definitions for the images to place in a page. Consider that
every page has got 8 rectangular areas arranged in table 4 rows by 2 columns.
From the REPL:

(save-svg-image #p"/path/to/svg/file/page-01.svg"
		(create-page #p"/path/to/dat/file/page-01.dat"
			     :debug t)
		:if-exists :supersede
		:debug t)

this will create the svg file reppresenting a single page.
