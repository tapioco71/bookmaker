;;;; bookmaker.lisp

(in-package #:bookmaker)

;;; "bookmaker" goes here. Hacks and glory await!

(define-condition file-not-found-warn (warning)
  ((file-pathname :initarg :file-pathname :reader file-pathname))
  (:report (lambda (condition stream)
	     (format stream "file not found: ~a~%" (file-pathname condition)))))

(define-condition file-already-exists-warn (warning)
  ((file-pathname :initarg :file-pathname :reader file-pathname))
  (:report (lambda (condition stream)
	     (format stream "image file already exists: ~a~%" (file-pathname condition)))))

(defstruct image-struct
  (name)
  (row)
  (column)
  (row-span)
  (column-span)
  (file-pathname))

(defstruct page-struct
  (name)
  (number)
  (background-color "red")
  (images-list))

(defparameter *version* "1.0")
(defparameter *conversion-dpi* 600)
(defparameter *unit* "mm")
(defparameter *page* '(146.0 209.0))
(defparameter *page-inside* '(2.0 2.5 142.0 204.0))
(defparameter *page-number-position* '(5.0 10.0))
(defparameter *table-span* '(4 2))
(defparameter *images-area* '(5.0 12.5 138.0 190.5))
(defparameter *image-borders* '(1.0 1.5)) 
(defparameter *binding-offset* 3.0)
(defparameter *frame-border-size* 0.5)

(defun number-with-unit (number &optional &key (unit *unit*))
  "Add a unit name to a number."
  (format nil "~a~a" number unit))

(defun create-page (page-file-pathname &optional &key (debug nil))
  "Create a single page using SVG drawings facility."
  (let ((page-data nil)
	(page-number nil)
	(page-number-position nil) 	
	(page (mapcar #'(lambda (x)
			  (number-with-unit x)) *page*))
	(page-inside nil)
	(scene nil))
    (with-open-file (page-file-stream page-file-pathname)
      (setq page-data (read page-file-stream)))
    (setq page-number (page-struct-number page-data))
    (setq page-number-string (format nil "~a" page-number))
    (if (eql (mod page-number 2) 0)
    	(setf page-number-position (list (first *page-number-position*)
    					 (second *page-number-position*))
    	      page-inside (list (first *page-inside*)
    				(second *page-inside*)
    				(- (third *page-inside*) 3.0)
    				(fourth *page-inside*)))
    	(setf page-number-position (list (- (third *page-inside*)
    					    (first *page-number-position*)
    					    3.0)
    					 (second *page-number-position*))
    	      page-inside (list (+ (first *page-inside*) 3.0)
    				(second *page-inside*)
    				(- (third *page-inside*) 3.0)
    				(fourth *page-inside*))))
    (setq scene (cl-svg:make-svg-toplevel 'cl-svg:svg-1.1-toplevel 
					  :width (first page) 
					  :height (second page)
					  :view-box (format nil "0mm 0mm ~a ~a" (first page) (second page))))
    (when scene
      (cl-svg:title scene (format nil "~a" (page-struct-name page-data)))
      (cl-svg:draw scene (:rect :x (number-with-unit (first page-inside))
    				:y (number-with-unit (second page-inside))
    				:width (number-with-unit (third page-inside))
    				:height (number-with-unit (fourth page-inside)) 
				:fill (page-struct-background-color page-data)))
      (cl-svg:text scene (:x (number-with-unit (first page-number-position)) :y (number-with-unit (second page-number-position)))
    	(cl-svg:tspan (:fill "black" :font-weight "bold" :font-size "18") page-number-string))
      (setq page-images-list (page-struct-images-list page-data))
      (let ((x 0)
    	    (y 0)
    	    (delta-x (/ (- (third *images-area*) 			  
    			   *binding-offset*)
    			(second *table-span*)))
    	    (delta-y (/ (fourth *images-area*)
    			(first *table-span*)))
    	    (width 0)
    	    (height 0))
	(when debug
	  (format *terminal-io* "Composing page named \"~a\" numbered ~a.~%" 
		  (page-struct-name page-data)
		  (page-struct-number page-data))
	  (format *terminal-io* "(delta-x, delta-y) = (~a, ~a)~%" delta-x delta-y))
    	(dolist (image (page-struct-images-list page-data))
    	  (setq x (+ (if (eql (mod page-number 2) 0)
    			 (- (/ *binding-offset* 2))
    			 *binding-offset*)
    		     (first *images-area*) 
    		     (first *image-borders*)
    		     (* (1- (image-struct-column image))
    			delta-x)))
    	  (setq y (+ (second *images-area*)
    		     (second *image-borders*)
    		     (* (1- (image-struct-row image))
    			delta-y)))
    	  (setq width (- (* (image-struct-column-span image)
    			    delta-x)
    			 (* 2
    			    (first *image-borders*))))
    	  (setq height (- (* (image-struct-row-span image)
    			     delta-y)
    			  (* 2
    			     (second *image-borders*))))
	  (when debug
	    (format *terminal-io* 
		    "Placing image named \"~a\" at (r, c) = (~a, ~a)~%(x, y) = (~a, ~a)~%(w, h) = (~a, ~a)~%"
		    (image-struct-name image)
		    (image-struct-row image)
		    (image-struct-column image)
		    x
		    y
		    width
		    height))
	  (unless (uiop:probe-file* (image-struct-file-pathname image))
	    (warn 'file-not-found-warn :file-pathname (image-struct-file-pathname image)))
    	  (cl-svg:draw scene (:image :x (number-with-unit x)
				     :y (number-with-unit y)
				     :width (number-with-unit width)
				     :height (number-with-unit height)	
				     :preserve-aspect-ration "none"
				     :xlink-href (image-struct-file-pathname image)))
    	  (cl-svg:draw scene (:rect :x (number-with-unit x)
				    :y (number-with-unit y)
				    :height (number-with-unit height)
				    :width (number-with-unit width)
				    :stroke "black"
				    :stroke-width (number-with-unit *frame-border-size*)
				    :fill "none")))))
    scene))

(defun create-pages (pages-directory-pathname &optional &key (if-exists nil) (debug nil))
  (let ((pages-count 0)
	(svg-page nil)
	(svg-file-pathname nil))
    (dolist (page-file-pathname (uiop:directory-files pages-directory-pathname))
      (when (string-equal (pathname-type page-file-pathname)
			  "dat")
	(setq svg-page (create-page page-file-pathname
				    :debug debug))
	(when svg-page
	  (setq svg-file-pathname (make-pathname :directory (pathname-directory page-file-pathname)
						 :name (pathname-name page-file-pathname)
						 :type "svg")) 
	  (when (save-svg-image svg-file-pathname
				svg-page
				:debug debug)
	    (incf pages-count)))))
    pages-count))

(defun search-image (directory-pathname &optional &key name row column row-span column-span file-pathname (debug nil))
  "Search an image among page descriptions files."
  (let* ((pages-directory-pathname (make-pathname :directory (pathname-directory directory-pathname)))
	 (pages-file-pathnames-list (uiop:directory-files pages-directory-pathname))
	 (found-pages-list nil))
    (dolist (page-file-pathname pages-file-pathnames-list)
      (when (string-equal (pathname-type page-file-pathname) 
			  "dat")
	(with-open-file (page-file-stream page-file-pathname)
	  (let* ((page-data (read page-file-stream))
		(result nil)
		(page-images-list (page-struct-images-list page-data)))
	    (when debug
	      (format *terminal-io* "Searching in file ~a: " (pathname-name page-file-pathname)))
	    (dolist (page-image page-images-list)
	      (when debug
		(format *terminal-io* "." page-image))
	      (setq result (and (if name
				    (string-equal name (image-struct-name page-image))
				    t)
				(if row
				    (eql row (image-struct-row page-image))
				    t)
				(if row-span
				    (eql row-span (image-struct-row-span page-image))
				    t)
				(if column-span
				    (eql coulmn-span (image-struct-column-span page-image))
				    t)
				(if file-pathname
				    (eql file-pathname (image-struct-file-pathname page-image))
				    t)))
	      (when result
		(when debug
		  (format *terminal-io* " Found in page description file: ~a" (pathname-name page-file-pathname)))
		(pushnew page-data found-pages-list)))
	    (when debug
	      (format *terminal-io* "~%"))))))
    (values found-pages-list
	    (length found-pages-list))))

(defun save-svg-image (svg-file-pathname svg-scene &optional &key (if-exists nil) (debug nil))
  "Save an SVG scene to a file."
  (when debug
    (format *terminal-io* "Saving svg file to ~a~%" svg-file-pathname))
  (with-open-file (svg-file-stream svg-file-pathname :direction :output :if-exists if-exists)
    (cl-svg:stream-out svg-file-stream 
		       svg-scene))
  t)

(defun change-pathname-type (source-pathname new-type)
  "Change file extension."
  (make-pathname :directory (pathname-directory source-pathname) :name (pathname-name source-pathname) :type new-type))

(defun convert-page (page-file-pathname &optional &key (if-exists nil) (debug nil))
  "Convert a single svg page document to pdf."
  (let ((result nil)
	(output nil)
	(error-output nil))
    (when (string-equal (pathname-type page-file-pathname) "svg")
      (if (uiop:probe-file* page-file-pathname)
	  (cond
	    ((and (eql if-exists nil)
		  (uiop:probe-file* (change-pathname-type page-file-pathname "pdf")))
	     (warn 'file-already-exists-warn :file-pathname (change-pathname-type page-file-pathname "pdf")))
	    ((eql if-exists :supersede)
	     (when debug	       
	       (format *terminal-io* "converting file: ~a to ~a~%" page-file-pathname (change-pathname-type page-file-pathname "pdf")))
	     (multiple-value-setq (output error-output result)
	       (uiop:run-program (format nil "inkscape ~a --export-pdf=~a --export-dpi=~a" 
					 page-file-pathname
					 (change-pathname-type page-file-pathname "pdf")
					 *conversion-dpi*)))))
	  (warn 'file-not-found-warn :file-pathname page-file-pathname)))
    result))
	 	      
(defun convert-pages (pages-directory-pathname &optional &key (if-exists nil) (debug nil))
  "Batch conversion from svg to pdf for files stored in a directory."
  (let ((pages-count 0))
    (dolist (page-file-pathname (uiop:directory-files pages-directory-pathname))
      (when (convert-page page-file-pathname :if-exists if-exists :debug debug)
	(incf pages-count)))
    pages-count))