;;;; bookmaker.asd

(asdf:defsystem #:bookmaker
  :description "Describe bookmaker here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :depends-on (#:hunchentoot
               #:cl-who
               #:cl-svg
               #:parenscript
	       #:retrospectiff
	       #:uiop)
  :serial t
  :components ((:file "package")
               (:file "bookmaker")))

